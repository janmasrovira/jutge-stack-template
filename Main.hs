module Main where

import           Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as B
import           Text.Megaparsec

main :: IO ()
main = do
  inp <- B.getContents
  print $ runParser myParser "stdin" inp

myParser :: Parsec ByteString [String]
myParser = do
  skipMany spaceChar -- preceding whitespaces
  ws <- many pWord   -- list of words
  eof                -- end of input
  return ws

pWord :: Parsec ByteString String
pWord = do
  w <- some letterChar
  skipMany spaceChar -- trailing whitespaces
  return w
